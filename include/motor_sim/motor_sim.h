#include<functional>
#include<Eigen/Dense>

struct EMotorStatus {
    float position = 0.0;
    float velocity = 0.0;
    float torque = 0.0;
};

class EMotor {
public:
    EMotor(float, bool);
    void bindCtrl(std::function<float(EMotorStatus)>);

    EMotorStatus getState();
    virtual void doSimStep() = 0;

protected:
    // simulation sampling time
    float dt;

    // motor variables
    EMotorStatus s;
    float curr;

    // motor parameters
    float J, b, K_e, K_t, R, L;

    // discretized state space model
    Eigen::Matrix2f Ad;
    Eigen::Vector2f Bd;

    // extenalize the controller
    std::function<float(EMotorStatus)> ctrl = nullptr;
};

class CurrentEMotor : public EMotor {
public:
    CurrentEMotor(float, bool);
    void doSimStep();
};

class VoltageEMotor : public EMotor {
public:
    VoltageEMotor(float, bool);
    void doSimStep();
};