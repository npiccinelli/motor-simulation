#include <motor_sim/motor_sim.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float32.h>
#include <teleop_msgs/SystemActuation.h>
#include <teleop_msgs/SystemStatus.h>
#include <chrono>

void refCallback(const teleop_msgs::SystemActuation::ConstPtr& msg, float& cmd)
{
    cmd = msg->command.data[0];
}

void distCallback(const teleop_msgs::SystemActuation::ConstPtr& msg, float& cmd)
{
    cmd = msg->command.data[0];
}

int main(int argc, char** argv)
{
    // node initialization
    ros::init(argc, argv, "motor_simulator");
    ros::NodeHandle nh = ros::NodeHandle("~");

    // retrive frequency parameter
    float dt;
    nh.param<float>("dt", dt, 0.01);
    auto loopRate = ros::Rate(1 / dt);

    // set control type
    float disturbe_ref = 0.0;
    float ref = 0.0;
    bool isClosedLoop = false;
    nh.param<bool>("closed_loop", isClosedLoop, false);

    std::string motorType;
    nh.param<std::string>("motor_type", motorType, "");
    for (auto& c : motorType) {
        c = tolower(c);
    }

    // motor instance
    EMotor* motor;
    if (motorType == "current") {
        motor = new CurrentEMotor(dt, isClosedLoop);
    } else if (motorType == "voltage") {
        motor = new VoltageEMotor(dt, isClosedLoop);
    }

    // definition of publisher, subscriber
    ros::Publisher jointStatePublisher = nh.advertise<sensor_msgs::JointState>("joint_states", 1);
    ros::Publisher sysStatePublisher = nh.advertise<teleop_msgs::SystemStatus>("state", 1);
    ros::Subscriber openRefSubscriber = nh.subscribe<teleop_msgs::SystemActuation>("refs", 1, std::bind(&refCallback, std::placeholders::_1, std::ref(ref)));
    ros::Subscriber disturbanceRefSubscriber = nh.subscribe<teleop_msgs::SystemActuation>("disturbance", 1, std::bind(&distCallback, std::placeholders::_1, std::ref(disturbe_ref)));

    // message initialization
    teleop_msgs::SystemStatus sysState;

    sensor_msgs::JointState robotState;
    robotState.name.resize(1);
    robotState.name[0] = "joint_0";
    robotState.position.resize(1);
    robotState.velocity.resize(1);
    robotState.effort.resize(1);

    // initializing motor controller
    float d_err = 0.0, i_err = 0.0, prev_err = 0.0;
    float Kp = 2.0;
    float Kd = 0.0;
    float Ki = 0.0;

    if (isClosedLoop) {
        motor->bindCtrl([&](EMotorStatus s) {
            float err = ref - s.position;
            d_err = (err - prev_err) / dt;
            i_err += err * dt;
            prev_err = err;

            return (Kp * err) + (Kd * d_err) + (Ki * i_err) + disturbe_ref;
        });
    } else {
        motor->bindCtrl([&](EMotorStatus s) {
            return ref + disturbe_ref;
        });
    }

    // main loop
    while (ros::ok()) {
        // update simulation
        motor->doSimStep();

        // retrive new state
        auto state = motor->getState();

        // publish in ROS as JointState message
        robotState.header.frame_id = "";
        robotState.header.stamp = ros::Time::now();
        robotState.position[0] = state.position;
        robotState.velocity[0] = state.velocity;
        robotState.effort[0] = state.torque;

        sysState.header.stamp = ros::Time::now();
        sysState.state = robotState;

        sysStatePublisher.publish(sysState);
        jointStatePublisher.publish(robotState);

        ROS_DEBUG_STREAM("New state (pos, vel, cmd): " << state.position << " " << state.velocity << " " << (ref + disturbe_ref));

        // spin messages
        ros::spinOnce();

        // sleep to match the desired update frequency
        bool rateOk = loopRate.sleep();
        if(!rateOk) {
            ROS_WARN("Unable to keep the desired update frequency");
        }
    }
}