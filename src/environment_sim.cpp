#include <interactive_markers/interactive_marker_server.h>
#include <ros/ros.h>
#include <teleop_msgs/SystemActuation.h>
#include <teleop_msgs/SystemStatus.h>
#include <tf/tf.h>
#include <thread>

#define OBSTACLE_INIT_ROT -M_PI_4

void action(const visualization_msgs::InteractiveMarkerFeedbackConstPtr& feedback, float& env_pos)
{
    tf::Matrix3x3 m(tf::Quaternion(
        feedback->pose.orientation.x,
        feedback->pose.orientation.y,
        feedback->pose.orientation.z,
        feedback->pose.orientation.w));
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    ROS_DEBUG_STREAM("Environment position updated: " << roll << " " << pitch << " " << yaw);

    env_pos = roll;
}

void makeMarker(interactive_markers::InteractiveMarkerServer& server, float& env_pos_ref)
{
    // create an interactive marker for our server
    visualization_msgs::InteractiveMarker int_marker;
    int_marker.header.frame_id = "slave/base_link";
    int_marker.pose.position.x = 0;
    int_marker.pose.position.y = 0;
    int_marker.pose.position.z = 0;
    int_marker.pose.orientation.x = tf::createQuaternionFromRPY(OBSTACLE_INIT_ROT, 0, M_PI_2).x();
    int_marker.pose.orientation.y = tf::createQuaternionFromRPY(OBSTACLE_INIT_ROT, 0, M_PI_2).y();
    int_marker.pose.orientation.z = tf::createQuaternionFromRPY(OBSTACLE_INIT_ROT, 0, M_PI_2).z();
    int_marker.pose.orientation.w = tf::createQuaternionFromRPY(OBSTACLE_INIT_ROT, 0, M_PI_2).w();
    int_marker.scale = 1;

    int_marker.name = "Environment at slave side";
    int_marker.description = "Environment position";

    // create a grey box marker
    visualization_msgs::Marker box_marker;
    box_marker.type = visualization_msgs::Marker::CYLINDER;
    box_marker.pose.position.z = -0.5;
    box_marker.scale.x = 0.1;
    box_marker.scale.y = 0.1;
    box_marker.scale.z = 1;
    box_marker.color.r = 0.0;
    box_marker.color.g = 0.0;
    box_marker.color.b = 0.0;
    box_marker.color.a = 0.8;

    // create a non-interactive control which contains the box
    visualization_msgs::InteractiveMarkerControl box_control;
    box_control.always_visible = true;
    box_control.markers.push_back(box_marker);

    // add the control to the interactive marker
    int_marker.controls.push_back(box_control);

    // create the position cursor
    visualization_msgs::InteractiveMarkerControl rotate_control;
    rotate_control.name = "rotate_environment";
    rotate_control.interaction_mode
        = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;

    // add the control to the interactive marker
    int_marker.controls.push_back(rotate_control);

    // add the interactive marker to our collection &
    // tell the server to call action() when feedback arrives for it
    server.insert(int_marker, std::bind(&action, std::placeholders::_1, std::ref(env_pos_ref)));

    // 'commit' changes and send to all clients
    server.applyChanges();
}

float refPos = 0.0;
float refVel = 0.0;

void refCallback(const teleop_msgs::SystemStatus::ConstPtr& msg)
{
    refPos = msg->state.position[0];
    refVel = msg->state.velocity[0];
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "environment");

    ros::NodeHandle nh = ros::NodeHandle("~");
    ros::Publisher envPublisher = nh.advertise<teleop_msgs::SystemActuation>("envAction", 1);
    ros::Subscriber refSubscriber = nh.subscribe<teleop_msgs::SystemStatus>("refStatus", 1, refCallback);

    // retrive frequency parameter
    float dt;
    nh.param<float>("dt", dt, 0.01);

    // create marker for rviz
    float envPos = OBSTACLE_INIT_ROT;

    // create an interactive marker server on the topic namespace environment
    interactive_markers::InteractiveMarkerServer server("environment");
    makeMarker(server, envPos);

    std::thread t([&]() {
        float Ke = 3.5; float Be = 0.9;
        teleop_msgs::SystemActuation msg;
        msg.command.data.resize(1);

        while (ros::ok()) {
            if (refPos <= envPos) {
                msg.command.data[0] = (Ke * std::fabs(refPos - envPos)) + (Be * refVel);
            } else {
                msg.command.data[0] = 0.0;
            }

            envPublisher.publish(msg);

            std::this_thread::sleep_for(std::chrono::milliseconds((int)(dt * 1000.0)));
        }
    });

    ros::spin();
}