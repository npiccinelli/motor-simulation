#include <motor_sim/motor_sim.h>
#include <iostream>

EMotor::EMotor(float _dt, bool _is_closed_loop)
{
    // sample time
    dt = _dt;

    //////// model constants
    // moment of inertia of the rotor (kg.m^2)
    J = 0.0266;
    // motor viscous friction constant (N.m.s)
    b = 0.0219;
    // electromotive force constant (V/rad/sec)
    K_e = 0.01;
    // motor torque constant (N.m/Amp)
    K_t = 0.0705;
    // electric resistance (Ohm)
    R = 1;
    // electric inductance (H)
    L = 0.5;

    //////// discretization using bilinear transformation (trapeziodal rule)
    Eigen::Matrix2f Ac;
    Eigen::Vector2f Bc;
    Ac << 0, 1, 0, -b / J;
    Bc << 0, 1 / J;
    Ad = (Eigen::Matrix2f::Identity() - (Ac * dt * 2.0f)).inverse() * (Eigen::Matrix2f::Identity() + (Ac * dt * 2.0f));
    Bd = (Eigen::Matrix2f::Identity() - (Ac * dt * 2.0f)).inverse() * Bc * dt;

    // initialize the system in steady state
    curr = 0.0;
}

EMotorStatus EMotor::getState()
{
    return s;
}

void EMotor::bindCtrl(std::function<float(EMotorStatus)> ctrl_func)
{
    ctrl = ctrl_func;
}