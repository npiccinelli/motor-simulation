#include <motor_sim/motor_sim.h>
VoltageEMotor::VoltageEMotor(float _dt, bool _is_closed_loop)
    : EMotor(_dt, _is_closed_loop)
{
}

void VoltageEMotor::doSimStep()
{
    // get the controller action
    float ref = ctrl(s);

    // compute the current variation based on the tension commanded
    auto d_curr = (1 / L) * ((-R * curr) + ref - K_e * s.velocity);
    curr = curr + (d_curr * dt);

    // applied torque
    s.torque = K_t * curr;

    // compute the acceleration based on the current variation
    auto acc = (1 / J) * ((K_t * curr) - (b * s.velocity));

    // update position and velocity
    s.velocity += acc * dt;
    s.position += s.velocity * dt;
}