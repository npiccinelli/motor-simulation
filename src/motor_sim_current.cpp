#include <motor_sim/motor_sim.h>

#define DISCRETE

CurrentEMotor::CurrentEMotor(float _dt, bool _is_closed_loop)
    : EMotor(_dt, _is_closed_loop)
{
}

void CurrentEMotor::doSimStep()
{
    // get the controller action
    float ref = ctrl(s);

    // applied torque
    s.torque = K_t * ref;

#ifdef DISCRETE
    // update position and velocity
    Eigen::Vector2f xcurr, xnext;
    xcurr(0) = s.position;
    xcurr(1) = s.velocity;

    xnext = (Ad * xcurr) + (Bd * s.torque);
    s.position = xnext(0);
    s.velocity = xnext(1);
#else
    // compute the acceleration based on the current variation
    auto acc = (1 / J) * ((K_t * ref) - (b * s.velocity));
    s.velocity += acc * dt;
    s.position += s.velocity * dt;
#endif
}